grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat)+ EOF!;

    
stat
    : expr NL -> expr
    | VAR ID (PODST expr)? SR -> ^(VAR ID) ^(PODST ID expr)?
    | ID PODST expr SR -> ^(PODST ID expr)
    | NL ->
    | ifexpr -> ifexpr
    ;


ifexpr : IF^ cond NL!? ifstat (NL!? elsestat)?;

cond : LP! expr RP! ;
ifstat : LB! expr RB! ;
elsestat : ELSE! NL!? ifstat ;


expr
      : multExpr 
      ( PLUS^ multExpr 
      | MINUS^ multExpr
      )* 
      ;

multExpr
      : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;
    
WHILE : 'while';

IF : 'if';

ELSE : 'else';

VAR : 'var';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n';

WS : (' '|'\t')+ {$channel = HIDDEN;} ;

SR : ';';

LB : '{' ;

RB : '}' ;

LP : '(' ;

RP : ')' ;

PODST: '=' ;

PLUS : '+' ;

MINUS :	'-' ;

MUL :	'*' ;

DIV :	'/' ;
