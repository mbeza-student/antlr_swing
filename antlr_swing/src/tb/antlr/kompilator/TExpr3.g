tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer ifCounter = 0;
  Integer conditionCounter = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e}, deklaracje={$d});

decl    : ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text});
        catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> calc(p1={$e1.st}, p2={$e2.st}, type={"ADD"})
        | ^(MINUS e1=expr e2=expr) -> calc(p1={$e1.st}, p2={$e2.st}, type={"SUB"})
        | ^(MUL   e1=expr e2=expr) -> calc(p1={$e1.st}, p2={$e2.st}, type={"MUL"})
        | ^(DIV   e1=expr e2=expr) -> calc(p1={$e1.st}, p2={$e2.st}, type={"DIV"})
        | ^(PODST i1=ID   e2=expr) -> set(id={$i1.text}, val={$e2.st})
        | ID                       -> get(id={$ID.text})
        | INT                      -> int(val={$INT.text})
        | ^(IF e1=expr s1=expr s2=expr?) {ifCounter++;} -> if(cond={$e1.st}, ifstat={$s1.st}, elstat={$s2.st}, count={ifCounter.toString()})
		    ;